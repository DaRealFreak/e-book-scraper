#!/usr/local/bin/python
# coding=utf-8
import os
import re

import html as html_module

import chapter_templating
import decorators
import html_beautifier
import session_mechanize
from chapter_parser import ChapterParser


class EBookScraper(object):
    """
    minimalistic ebook scraper which most likely won't work on most pages yip yip hooray
    """

    extract_chapterlist = None
    chapter_parser = None
    chapterlist_pattern = None
    working_directory = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, "tests"))
    chapterlist_url = ""
    book_title = ""
    book_volume = ""
    book_alternative_title = ""

    def __init__(self):
        """
        initializing function
        """
        self.session = session_mechanize.MechanizeSession()
        self.html_beautifier = html_beautifier.HtmlBeautifier()

    def set_chapterlist_url(self, url):
        self.chapterlist_url = url

    def set_chapterlist_pattern(self, chapterlist_pattern):
        self.chapterlist_pattern = chapterlist_pattern

    def set_working_directory(self, directory):
        self.working_directory = os.path.abspath(directory)

    def set_book_title(self, title):
        self.book_title = html_module.escape(title)

    def set_book_alternative_title(self, alternative_title):
        self.book_alternative_title = html_module.escape(alternative_title)

    def set_book_volume(self, volume):
        self.book_volume = html_module.escape(volume)

    def set_extract_chapterlist_method(self, obj):
        """
        well the chapterlist is a bitch and most likely BeautiulSoup or some domtree work, so we
        use an reference to retrieve it, needs to be maintained by the user

        :param obj:
        :return:
        """
        self.extract_chapterlist = obj

    def set_extract_chapter_method(self, obj):
        """
        add the custom chapter parser

        :param obj:
        :return:
        """
        self.chapter_parser = ChapterParser(obj)

    def scrap(self, chapter_pattern, title_pattern):
        """
        scrap it

        :param chapter_pattern:
        :param title_pattern:
        :return:
        """
        templating_engine = chapter_templating.ChapterTemplateEngine(chapter_pattern, title_pattern,
                                                                     self.html_beautifier)

        if not self.chapterlist_pattern or not self.extract_chapterlist or not self.book_title:
            return False

        # no response no chapter ¯\_(ツ)_/¯
        res = self.session.open(self.chapterlist_url)
        if not res:
            return False

        content = res.read()
        chapterlist_urls = self.chapterlist_pattern.findall(content)
        chapterlist = self.extract_chapterlist(content)
        content_xhtml = templating_engine.get_content_xhtml(self.book_title, chapterlist, self.book_alternative_title,
                                                            self.book_volume)
        templates = templating_engine.xhtml_template_creation(chapterlist)
        association = {}
        for template in templates:
            association[template["chapter_index"]] = template
        for url, index in chapterlist_urls:
            key = index.zfill(4)
            if key in association:
                association[key]["url"] = url
            else:
                print u'Chapter key "{0:s}" not found in association, check if special chapter'.format(key)
        self.create_templates(content_xhtml, association)

    def create_templates(self, content_xhtml, association):
        """
        create the templates we already generated physically on the drive

        :param content_xhtml:
        :param association:
        :return:
        """
        if not os.path.exists(self.working_directory) and os.path.isdir(self.working_directory):
            os.makedirs(self.working_directory)

        open(os.path.join(self.working_directory, u"content.xhtml"), "w").write(
            self.html_beautifier.beautify(content_xhtml).encode('utf-8', 'ignore'))

        for chapter_index in association:
            chapter = association[chapter_index]
            filename = os.path.join(self.working_directory, chapter["filename"])
            template = chapter["content"]
            if "url" not in chapter:
                print u'Chapter key "{0:s}" not has no url associated, check if special chapter'.format(chapter_index)
                continue
            url = chapter["url"]
            self.chapter_parser.parse(url)
            chapter_content = self.chapter_parser.html
            chapter_xhtml = template % chapter_content
            open(filename, "w").write(self.html_beautifier.beautify(chapter_xhtml).encode('utf-8', 'ignore'))


if __name__ == '__main__':
    from bs4 import BeautifulSoup


    @decorators.listify()
    def extract_wuxiaworld_chapterlist(html_content):
        """
        why the fuck is wuxiaworld soup not working like in chrome console???
        using regular expressions for html parsing for now(don't do this at home kids!)

        :param html_content:
        :return:
        """
        p = re.compile('<a href="http://www.wuxiaworld.com/tdg-index/tdg-chapter-\d+/">(Chapter.*\d+[^<]+)</a>',
                       re.IGNORECASE)
        results = p.findall(html_content)
        for result in results:
            yield html_module.escape(result.replace('\xe2\x80\x93', "-").replace('\xc2\xa0', " "))


    def extract_wuxiaworld_chapter(html_content):
        chapterline_pattern = re.compile("Chapter \d+.*", re.IGNORECASE)
        soup = BeautifulSoup(html_content, "html.parser")
        article_body = soup.find("div", {"itemprop": "articleBody"})
        if str(article_body).count("<hr/>") + str(article_body).count("<hr>") == 2:
            article_body = str(article_body).replace("<hr>", "<hr/>")
            chapter_text = BeautifulSoup(str(article_body).split("<hr/>")[1], "html.parser").text
        else:
            print re.findall('Chapter (\d+) -', html_content)
            chapter_text = article_body.text
        for line in chapter_text.splitlines():
            if re.match(chapterline_pattern, line):
                chapter_text = chapter_text.replace(line, u"")
        return chapter_text.strip()


    ebook_scraper = EBookScraper()

    # example wuxiaworld
    wuxiaworld_chapterlist_pattern = re.compile('(http://www.wuxiaworld.com/tdg-index/tdg-chapter-(\d+)/)',
                                                re.IGNORECASE)
    wuxiaworld_chapter_pattern = re.compile('Chapter (\d+)', re.IGNORECASE)
    wuxiaworld_title_pattern = re.compile('(Chapter \d+ - .*)', re.IGNORECASE)

    ebook_scraper.set_book_title(u"Tales of Demons and Gods")
    ebook_scraper.set_book_alternative_title(u"妖神记")
    ebook_scraper.set_book_volume(u"Volume 01")
    ebook_scraper.set_chapterlist_url("http://www.wuxiaworld.com/tdg-index/")
    ebook_scraper.set_chapterlist_pattern(wuxiaworld_chapterlist_pattern)
    ebook_scraper.set_extract_chapterlist_method(extract_wuxiaworld_chapterlist)
    ebook_scraper.set_extract_chapter_method(extract_wuxiaworld_chapter)
    ebook_scraper.scrap(wuxiaworld_chapter_pattern, wuxiaworld_title_pattern)
