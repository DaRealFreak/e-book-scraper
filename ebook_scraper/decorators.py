#!/usr/local/bin/python
# coding=utf-8
import functools


def singleton(class_):
    """
    singleton wrapper instead of meta

    :param class_:
    :return:
    """
    class ClassWrap(class_):
        _instance = None

        def __new__(cls, *args, **kwargs):
            if ClassWrap._instance is None:
                ClassWrap._instance = super(ClassWrap, cls).__new__(cls, *args, **kwargs)
                ClassWrap._instance._sealed = False
            return ClassWrap._instance

        def __init__(self, *args, **kwargs):
            if self._sealed:
                return
            super(ClassWrap, self).__init__(*args, **kwargs)
            self._sealed = True

    ClassWrap.__name__ = class_.__name__
    return ClassWrap


def listify(fn=None, wrapper=list):
    """
    A decorator which wraps a function's return value in ``list(...)``.

    Useful when an algorithm can be expressed more cleanly as a generator but
    the function should return an list.

    Example::

        >>> @listify
        ... def get_lengths(iterable):
        ...     for i in iterable:
        ...         yield len(i)
        >>> get_lengths(["spam", "eggs"])
        [4, 4]
        >>>
        >>> @listify(wrapper=tuple)
        ... def get_lengths_tuple(iterable):
        ...     for i in iterable:
        ...         yield len(i)
        >>> get_lengths_tuple(["foo", "bar"])
        (3, 3)
    """

    # noinspection PyShadowingNames
    def listify_return(fn):
        @functools.wraps(fn)
        def listify_helper(*args, **kw):
            return wrapper(fn(*args, **kw))

        return listify_helper

    if fn is None:
        return listify_return
    return listify_return(fn)
