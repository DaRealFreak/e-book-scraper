#!/usr/local/bin/python
# coding=utf-8

from ebook_scraper import session_mechanize
import html as html_module


class ChapterParser(object):
    _raw_html = u""
    _html = u""
    _body = u""
    replace_dict = {
        u"\n": u"<br/>\n"
    }
    extract_chapter_method = None

    def __init__(self, extract_chapter_method):
        """
        initializing function

        :param extract_chapter_method:
        """
        self.session = session_mechanize.MechanizeSession()
        self.extract_chapter_method = extract_chapter_method

    def parse(self, url):
        """
        parsing function

        :param url:
        :return:
        """
        self._raw_html = u""
        self._html = u""
        self._body = u""
        res = self.session.open(url)
        if res:
            self._raw_html = res.read()

    @property
    def body(self):
        if self.extract_chapter_method and not self._body:
            self._body = self.extract_chapter_method(self._raw_html)
        return self._body

    @property
    def html(self):
        if not self._html:
            html = self.body
            html = html_module.escape(html)

            for character in self.replace_dict:
                html = html.replace(character, self.replace_dict[character])
            self._html = html
        return self._html


if __name__ == '__main__':
    from bs4 import BeautifulSoup
    import re


    def extract_wuxiaworld_chapter(html_content):
        chapterline_pattern = re.compile(u"Chapter \d+.*", re.IGNORECASE)
        soup = BeautifulSoup(html_content, "html.parser")
        article_body = soup.find("div", {"itemprop": "articleBody"})
        if str(article_body).count("<hr/>") + str(article_body).count("<hr>") == 2:
            article_body = str(article_body).replace("<hr>", "<hr/>")
            chapter_text = BeautifulSoup(str(article_body).split("<hr/>")[1], "html.parser").text
        else:
            chapter_text = article_body.text
        for line in chapter_text.splitlines():
            if re.match(chapterline_pattern, line):
                chapter_text = chapter_text.replace(line, "")
        return chapter_text.strip()


    chapter_parser = ChapterParser(extract_wuxiaworld_chapter)
    chapter_parser.parse("http://wuxiaworld.com/tdg-index/tdg-chapter-18/")
    print chapter_parser.html
