#!/usr/local/bin/python
# coding=utf-8
import cookielib
import random
import warnings

import mechanize

# i should decide on a proper mail name in the future, still has time
__author__ = 'DaRealFreak <dasbaumchen@web.de>'


# noinspection PyShadowingBuiltins
class MechanizeSession(object):
    browser = None

    def __init__(self):
        self.proxy = None
        self.create_session()

    def __getattr__(self, name):
        """
        generic methods to call onto the browser object
        to guarantee full functionality of the specific session

        :type name: str
        :return:
        """
        if name in dir(self.browser):
            return getattr(self.browser, name)
        else:
            err = "'%s' object has no attribute '%s'" % (self.__class__.__name__, name)
            raise AttributeError(err)

    def __setitem__(self, key, value):
        """
        generic variable setter for the browser object
        to guarantee full functionality of the specific session

        :param key:
        :param value:
        :return:
        """
        try:
            setattr(self.browser, key, value)
        except Exception as err:
            print err

    def __getitem__(self, key):
        """
        generic variables to retrieve from the browser object
        to guarantee full functionality of the specific session

        :type key: str
        :return:
        """
        if key in dir(self.browser):
            try:
                return getattr(self.browser, key)
            except:
                err = "attribute '%s' could not get returned from '%s'" % (key, self.__class__.__name__)
                raise AttributeError(err)
        else:
            err = "'%s' object has no attribute '%s'" % (self.__class__.__name__, key)
            raise AttributeError(err)

    def get_session(self):
        """
        getter for the session

        :return:
        """
        return self.browser

    @staticmethod
    def get_random_useragent():
        """
        copypasted from here:
        http://teh-1337.studiobebop.net/blog.py?post=204
        don't like the thousand function calls but whatever

        :return:
        """
        base_agent = "Mozilla/%.1f (Windows; U; Windows NT 5.1; en-US; rv:%.1f.%.1f) Gecko/%d0%d Firefox/%.1f.%.1f"
        return base_agent % ((random.random() + 5), (random.random() + random.randint(1, 8)), random.random(),
                             random.randint(2000, 2100), random.randint(92215, 99999),
                             (random.random() + random.randint(3, 9)), random.random()
                             )

    def set_user_agent(self, agent=None):
        """
        sets the user agent, if none is defined, generate a valid random one

        :param agent:
        :return:
        """
        if agent:
            self.browser.addheaders = [("User-agent", agent)]
        else:
            self.browser.addheaders = [("User-agent", self.get_random_useragent())]
        self.set_accept_headers()

    def get_user_agent(self):
        """
        returns the header of user-agent

        :return:
        """
        for header in self.browser.addheaders:
            if header[0].lower() == "user-agent":
                return header[1]

    def set_accept_headers(self):
        """
        add the accept headers

        :return:
        """
        self.browser.addheaders.append(('accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'))
        self.browser.addheaders.append(('accept-language', 'en-US,de-DE;q=0.7,en;q=0.3'))
        self.browser.addheaders.append(('accept-encoding', 'gzip'))

    def get_headers(self):
        """
        returns all headers

        :return:
        """
        return self.browser.addheaders

    def create_session(self):
        """
        creates the session and defines the most common settings predefined

        :return:
        """
        cj = cookielib.LWPCookieJar()
        self.browser = mechanize.Browser()
        self.browser.set_cookiejar(cj)
        self.browser.set_handle_equiv(True)
        # ignore the experimental warning, we don't need it here
        warnings.filterwarnings('ignore', 'gzip transfer encoding is experimental!')
        self.browser.set_handle_gzip(True)
        self.browser.set_handle_redirect(True)
        self.browser.set_handle_referer(True)
        self.browser.set_handle_robots(False)
        # noinspection PyProtectedMember
        self.browser.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)
        self.set_user_agent()


instance = MechanizeSession

if __name__ == '__main__':
    mechanize_session = MechanizeSession()
    print mechanize_session.open("https://www.google.de").read()
    print mechanize_session.get_user_agent()
    print mechanize_session.get_session().open("https://www.google.de").read()

    image_test = True
    if image_test:
        open("test.gif", "wb").write(
            mechanize_session.open("http://i1142.photobucket.com/albums/"
                                   "n615/tapuy/hestia2-PJM_zps2ag0zzve.gif").read()
        )
