#!/usr/local/bin/python
# coding=utf-8
import abc
import re


class ChapterTemplate(object):
    __metaclass__ = abc.ABCMeta

    # maybe a bit overkill to use this as a template...
    file_name_template = u"chapter{0:s}.xhtml"

    content_template = u"""<?xml version='1.0' encoding='utf-8'?>
                          <html xmlns="http://www.w3.org/1999/xhtml">
                              <head>
                                  <title>Table of Contents</title>
                                  <link type="text/css" rel="stylesheet" media="all" href="../stylesheet.css"/>
                              </head>
                              <body>
                                  <div>
                                      <h3>{0:s}</h3>
                                      <h4><i>{1:s}</i></h4>
                                      <h3 style="font-weight:normal;">{2:s}</h3>
                                      <br/>
                                      <div class="left" style="text-align:left;text-indent:0;">
                                          {3:s}
                                      </div>
                                  </div>
                              </body>
                          </html>"""

    # yeaah blame me, do it!
    content_list_template = u"""<p><a href="{0:s}">
                            {1:s}
                        </a></p>\n"""

    # we are using both possible string formatting options here so we can fill the usual metadata
    # like chapter number and chapter title beforehand and the actual chapter later
    xhtml_template = u"""<?xml version='1.0' encoding='utf-8'?>
                        <html xmlns="http://www.w3.org/1999/xhtml">
                            <head>
                                <title>Chapter {0:s}</title>
                                <link type="text/css" rel="stylesheet" media="all" href="../stylesheet.css"/>
                            </head>
                            <body>
                                <div class="left" style="text-align:left;text-indent:0;">
                                    <h3>
                                        {1:s}
                                    </h3>
                                    <hr/>
                                    %s
                                </div>
                            </body>
                        </html>"""


class ChapterTemplateEngine(ChapterTemplate):
    """
    minimalistic template engine because I don't want this shit in my code
    """

    def __init__(self, chapter_pattern, title_pattern, beautifier):
        """
        initializing function

        :param chapter_pattern:
        :param title_pattern:
        :param beautifier:
        """
        super(ChapterTemplateEngine, self).__init__()
        self._chapter_pattern = chapter_pattern
        self._title_pattern = title_pattern
        self.html_beautifier = beautifier

    @property
    def chapter_pattern(self):
        return self._chapter_pattern

    @property
    def title_pattern(self):
        return self._title_pattern

    def xhtml_template_creation(self, chapterlist):
        """
        create the basic xhtml templates for the pages without any content so far

        :param chapterlist:
        :return:
        """
        for chapter in chapterlist:
            if not chapter:
                continue

            # if we can't find any chapter number I don't see any sense in continuing, most likely
            # false data from the website
            chapter_number = self._chapter_pattern.findall(chapter)
            if not chapter_number:
                continue
            else:
                chapter_number = chapter_number[0]

            # if we can't find a title, use a generic title
            chapter_titles = self._title_pattern.findall(chapter)
            if chapter_titles:
                chapter_title = chapter_titles[0]
            else:
                chapter_title = u"Chapter {0:s}".format(chapter_number)

            filename = self.file_name_template.format(chapter_number.zfill(4))
            yield {
                "chapter_index": chapter_number.zfill(4),
                "filename": filename,
                "content": self.html_beautifier.beautify(
                    self.xhtml_template.format(chapter_number, chapter_title.decode('utf-8'))
                )
            }

    def get_content_xhtml(self, name, chapterlist, alternative_title=u'', volume=u''):
        """
        create the content overview for the different pages including the links

        :param name:
        :param chapterlist:
        :param alternative_title:
        :param volume:
        :return:
        """
        content_xhtml = u""
        for chapter in chapterlist:
            if not chapter:
                continue

            chapter_number = self._chapter_pattern.findall(chapter)
            if not chapter_number:
                continue
            else:
                chapter_number = chapter_number[0]

            # if we can't find a title, use a generic title
            chapter_titles = self._title_pattern.findall(chapter)
            if chapter_titles:
                chapter_title = chapter_titles[0]
            else:
                chapter_title = u"Chapter {0:s}".format(chapter_number)
            content_xhtml += self.content_list_template.format(
                self.file_name_template.format(chapter_number.zfill(4)),
                chapter_title.decode('utf-8'))
        content_xhtml = self.content_template.format(name, alternative_title, volume, content_xhtml)
        return self.html_beautifier.beautify(content_xhtml)


if __name__ == '__main__':
    from ebook_scraper.html_beautifier import HtmlBeautifier

    example_chapter_pattern = re.compile('Chapter (\d+) ', re.IGNORECASE)
    example_title_pattern = re.compile('(Chapter \d+ - .*)', re.IGNORECASE)
    html_beautifier = HtmlBeautifier()

    templating_engine = ChapterTemplateEngine(example_chapter_pattern, example_title_pattern, html_beautifier)
    example_chapterlist = open("../tests/chapter_list.txt").readlines()
    print templating_engine.get_content_xhtml(u"Tales of Demons and Gods", example_chapterlist, u"妖神记", u"Volume 01")
    for template in templating_engine.xhtml_template_creation(example_chapterlist):
        print template["filename"]
        print template["content"]
