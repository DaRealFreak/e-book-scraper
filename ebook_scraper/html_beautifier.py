#!/usr/local/bin/python
# coding=utf-8
import re

from bs4 import BeautifulSoup


class HtmlBeautifier(object):
    """
    basic identation is 1 space, don't want it, I like 4 spaces!
    """

    def __init__(self):
        """
        initializing function
        """
        self.beautified = True
        orig_prettify = BeautifulSoup.prettify
        r = re.compile(r'^(\s*)', re.MULTILINE)

        def prettify(self, encoding=None, formatter="minimal", indent_width=4):
            """
            just replace the damn function

            :param self:
            :param encoding:
            :param formatter:
            :param indent_width:
            :return:
            """
            return r.sub(r'\1' * indent_width, orig_prettify(self, encoding, formatter))

        BeautifulSoup.prettify = prettify

    # noinspection PyMethodMayBeStatic
    def beautify(self, html):
        """
        I feel like the class would be useless if I would just finish everything on the initialize
        sooo we got a pretty little function which does nothing else than to restrict our possible arguments
        and sounds cooler than prettify!

        :param html:
        :return:
        """
        return BeautifulSoup(html, "html.parser").prettify()


if __name__ == '__main__':
    html_beautifier = HtmlBeautifier()
    content = open("../tests/chapter_unformatted.html").read()
    print html_beautifier.beautify(content)
