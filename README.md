# E-Book Scraper

minimalistic e-book scraper


### Version
0.0.1

### Dependencies

The e-book scraper uses only cross-platform available third party libraries to provide functionality on all platforms.

Required:

* [mechanize] - implementation of urllib2.OpenerDirector interface
* [BeautifulSoup] - HTML DOM Parser for Python


### Usage

```sh

```

### Development

Want to contribute? Great!
I'm always glad hearing about bugs or pull requests.

### ToDos

 - nothing here
  
License
----

GPL


   [mechanize]: <https://pypi.python.org/pypi/mechanize>
   [BeautifulSoup]: <https://www.crummy.com/software/BeautifulSoup/>
      